﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using System.IO;
using RestSharp;
using System.Configuration;


namespace CheckJira
{
    class Program
    {
        static string url = "https://astroradio.atlassian.net/rest/api/2";
        static int maxResult = 100;
        static string email = ConfigurationManager.AppSettings["email"];
        static string password = ConfigurationManager.AppSettings["password"];

        static RestResponse GetJiraResponse(string query, RestSharp.Method method)
        {
            var client = new RestClient(url);
            var request = new RestRequest(query, method);
            request.AddHeader("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(email + ":" + password)));

            return (RestResponse)client.Execute(request);
        }

        static List<string> GetProjects()
        {
            List<string> result = new List<string>();

            RestResponse resp = GetJiraResponse("project", Method.GET);

            string a = "{\"projects\":" + resp.Content + "}";

            JObject jobj = JObject.Parse(a);
            foreach (var x in jobj["projects"])
            {
                result.Add(x["key"].ToString());

            }
            return result;
        }

        static RestResponse GetJobs(string project_key, int startAt)
        {
            return GetJiraResponse("search?jql=project=" + project_key + "&fields=attachment,parent,issuetype,created,status,issuelinks,updated&maxResults=" + maxResult + "&startAt=" + (startAt * maxResult).ToString(), Method.GET);
        }

        static int GetTotal(string project_key)
        {
            RestResponse response = GetJiraResponse("search?jql=project=" + project_key + "&&fields=*none&maxResults=0", Method.GET);

            return Convert.ToInt32(JObject.Parse(response.Content)["total"]);
        }

        static void AppendFile(string line)
        {
            using (StreamWriter sw = new StreamWriter("result.csv", true))
            {
                sw.WriteLine(line);
            }
        }

        static RestResponse GetAOJ(int startAt)
        {
            return GetJiraResponse("search?jql=project=FJ&fields=attachment,parent,issuetype,created,status,updated&maxResults=100&startAt=" + (startAt * 100).ToString(), Method.GET);
        }

        static void Main(string[] args)
        {
            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("Please fill in the email and password in config file!");
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
            else
            {
                try
                {
                    AppendFile("project,key,created,updated,status,filename,file_size,file_created,file_type");
                    List<string> projects = GetProjects();

                    foreach (string project in projects)
                    {
                        int total_issue = GetTotal(project);
                        Console.Write("Accessing " + project + "\t | total issue: " + total_issue);

                        int maxStartAt = (total_issue + maxResult - 1) / maxResult;
                        for (int i = 0; i < maxStartAt; i++)
                        {
                            RestResponse resp = GetJobs(project, i);
                            JObject jobj = JObject.Parse(resp.Content);
                            foreach (var x in jobj["issues"])
                            {
                                string key = x["key"].ToString();
                                string created = x["fields"]["created"].ToString();
                                string updated = x["fields"]["updated"].ToString();
                                string status = x["fields"]["status"]["name"].ToString();
                                JToken attachments = x["fields"]["attachment"];
                                foreach (var y in attachments)
                                {
                                    string filename = y["filename"].ToString();
                                    int size = Convert.ToInt32(y["size"]);
                                    string typess = y["mimeType"].ToString();
                                    string file_created = y["created"].ToString();
                                    AppendFile(project + "," + key + "," + created + "," + updated + "," + status + ",\"" + filename + "\"," + size + "," + file_created + "," + typess);
                                }
                            }
                            Console.Write("\rAccessing " + project + "\t | total issue: " + total_issue + "\t  | Complete " + Math.Min((i + 1) * maxResult, total_issue));
                        }
                        Console.WriteLine();
                    }
                }
                catch
                {
                    Console.WriteLine("Error! Please make sure if the email and password are correct.");
                }
            }
        }
    }
}
